## Settings

```
{
    "workbench.colorTheme": "Material Neutral",
    "editor.fontFamily": "RobotoMono Nerd Font",
    "editor.fontSize": 12,
    "editor.tabSize": 2,
    "editor.lineHeight": 16,
    "git.enabled": true,
    "explorer.openEditors.visible": 0,
    "editor.scrollBeyondLastLine": false,
    "editor.renderWhitespace": "boundary",
    "git.confirmSync": false,
    "editor.cursorStyle": "line",
    "telemetry.enableTelemetry": false,
    "editor.wordWrap": "on",
    "problems.decorations.enabled": false,
    "editor.folding": true,
    "editor.renderIndentGuides": false,
    "editor.trimAutoWhitespace": false,
    "editor.cursorBlinking": "solid",
    "diffEditor.ignoreTrimWhitespace": false,
    "workbench.colorCustomizations": {
      "editor.selectionBackground": "#000000f1",
      "editor.selectionForeground": "#000",
      "editor.inactiveSelectionBackground": "#000000f1",
      "editor.selectionHighlightBackground": "#000000f1"
    },
  
    "shortcuts.buttons": [
      "terminal , workbench.action.terminal.toggleTerminal , Toggle terminal panel",
      "telescope , workbench.action.showCommands , Show command palette"
    ],
  
    "workbench.iconTheme": "material-icon-theme",
    "terminal.integrated.shell.osx": "/usr/local/bin/fish",
    "workbench.startupEditor": "newUntitledFile",
    "git.autofetch": true,
    "window.zoomLevel": 0
  }
```

### Extensions

- Material Neutral | ext install theme-material-neutral
- Git History | ext install githistory
- Material Icon Theme | ext install material-icon-theme
- Babel | ext install vscode-babel-coloring
- Rainbow Brackets | ext install rainbow-brackets
- Git Blame | ext install gitblame
- Drupal Syntax Highlighting | ext install VS-code-drupal
- Path Intellisense | ext install path-intellisense
- beautify | ext install beautify
- minify | ext install minify
- markdownlint | ext install vscode-markdownlint
- Annotator | ext install annotator
- Shortcuts | ext install shortcuts
- Output Colorizer | ext install output-colorizer
- Polacode
- Open in github/etc